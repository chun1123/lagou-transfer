# 作业一
## 本工程实现了简单的Spring Annotation功能，
提供的注解有@MyAutowired，@MyComponent,@MyTransactional,其中MyComponent与spring的Cmponent注解
功能一致，MyAutowired与Spring的Autowired注解功能一致，MyTransactional与Spring的Transactional注解功能
一致。@MyTransactional注解添加在类上与添加在方法上均能实现事务管理的功能。
## 简单代理的实现，ProxyFactory会根据被代理对象是否实现了接口来决定是使用jdkProxy还是
cglibProxy代理；

# 作业二
作业二的地址为
https://www.processon.com/view/link/5e1190fae4b0bcfb73332df7，
同时存了一份本地文件在SequenceGraph目录下SequenceGraph/spring bean实例化流程.png