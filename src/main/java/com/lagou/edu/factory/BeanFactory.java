package com.lagou.edu.factory;

import com.alibaba.druid.util.StringUtils;
import com.lagou.edu.annotation.MyAutowired;
import com.lagou.edu.annotation.MyComponent;
import com.lagou.edu.annotation.MyTransactional;
import com.lagou.edu.service.TransferService;
import com.lagou.edu.utils.MyStringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @author 应癫
 * <p>
 * 工厂类，生产对象（使用反射技术）
 */
public class BeanFactory {
    private static List<String> classPathClassNameLists = new ArrayList<>();
    private static Map<String, Class> myAnnotationClazzMap = new HashMap<>();  // 存储classname->对象

    /**
     * 任务一：读取解析xml，通过反射技术实例化对象并且存储待用（map集合）
     * 任务二：对外提供获取实例对象的接口（根据id获取）
     */

    private static Map<String, Object> map = new HashMap<>();  // 存储id->对象
    private static Map<String, Object> clazzmap = new HashMap<>();  // 存储classname->对象
    private static Map<String, String> classNameIdMap = new HashMap<>();  // 存储classname->对象
    private static Map<String, Object> clazzNeedsProxy = new HashMap<>();

    private static Set<String> resolvedBean = new HashSet<String>();

    static {
//        resolveBeanXML();
        resolveBeanAnnotation();
//
    }

    private static void resolveBeanAnnotation() {
        ClassLoader classLoader = BeanFactory.class.getClassLoader();
        String resources = classLoader.getResource("").getPath();
        File classPathFile = new File(resources);

        getAllClassFile(classPathFile);
        filterMyAnnotationClass();
        initializeMyBean();
        postProcessAfterInitializeMyBean();
    }

    private static void postProcessAfterInitializeMyBean() {
        filterNeedsProxyClass();
        createProxy();
    }

    private static void createProxy() {
        clazzNeedsProxy.forEach((classname, clazz)-> {
            Object o = clazzmap.get(classname);
            ProxyFactory proxyFactory = (ProxyFactory) BeanFactory.getBean("proxyFactory");
            Object proxy  = proxyFactory.getProxy(o) ;
            clazzmap.put(classname, proxy);
            map.put(classNameIdMap.get(classname), proxy);
        });
    }

    private static void filterNeedsProxyClass() {
        for (String className : classPathClassNameLists) {
            try {
                Class clazz = getClassLoader().loadClass(className);

                Annotation[] annotations = clazz.getAnnotations();
                for (Annotation annotation : annotations) {
                    if (annotation instanceof MyTransactional) {
                        clazzNeedsProxy.put(className, clazz);
                        continue;
                    }
                }
                Method[] methods = clazz.getDeclaredMethods();
                for (Method method : methods) {
                    Annotation[] methodAnnotations = method.getAnnotations();
                    for (Annotation annotation : methodAnnotations) {
                        if (annotation instanceof MyTransactional) {
                            clazzNeedsProxy.put(className, clazz);
                            continue;
                        }
                    }
                }
                System.out.println(className);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // 任务二：对外提供获取实例对象的接口（根据id获取）
    public static Object getBean(String id) {
        return map.get(id);
    }

    public static Object getBean(Class clazz) {
        return clazzmap.get(clazz.getName());
    }

    public static void main(String[] args) {
        resolveBeanAnnotation();
        System.out.println(myAnnotationClazzMap);
        System.out.println(map);

        ProxyFactory proxyFactory = (ProxyFactory) BeanFactory.getBean("proxyFactory");
        TransferService transferService = (TransferService) proxyFactory.getJdkProxy(BeanFactory.getBean("transferService"));
        System.out.println(transferService);
    }

    private static void initializeMyBean() {
        myAnnotationClazzMap.forEach((classname, clazz) -> {
            try {
                if (!resolvedBean.contains(classname)) {
                    initializeClazz(clazz);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
    }

    private static void initializeClazz(Class clazz) throws IllegalAccessException {
        Object o = null;
        try {
            o = clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        String id = "";
        Annotation[] annotations = clazz.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof MyComponent) {
                MyComponent mycomponent = (MyComponent) annotation;
                id = mycomponent.value();
            }
        }
        if (StringUtils.isEmpty(id)) {
            id = MyStringUtils.toLowerCase4Index(clazz.getSimpleName());
        }
        map.put(id, o);
        clazzmap.put(clazz.getName(), o);
        classNameIdMap.put(clazz.getName(), id);
        resolveAutowiredProperties(clazz, o);

        map.put(id, o);
        clazzmap.put(clazz.getName(), o);
        resolvedBean.add(clazz.getName());
    }

    private static void filterMyAnnotationClass() {
        for (String className : classPathClassNameLists) {
            try {
                Class clazz = getClassLoader().loadClass(className);

                Annotation[] annotations = clazz.getAnnotations();
                for (Annotation annotation : annotations) {
                    if (annotation instanceof MyComponent) {
//                        Type[] types = clazz.getGenericInterfaces();
                        myAnnotationClazzMap.put(className, clazz);
//                        resovleBeanWithMySpringAnnnotions(annotation, clazz);
                    }
                }
                System.out.println(className);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static ClassLoader getClassLoader() {
        return BeanFactory.class.getClassLoader();
    }

    private static boolean mySpringAnnotionPresents(Annotation[] annotations) {
        return false;
    }

    private static void resovleBeanWithMySpringAnnnotions(Annotation annotation, Class clazz) throws IllegalAccessException, InstantiationException {
//        Object o = clazz.newInstance();
//        MyComponent mycomponent = (MyComponent) annotation;
//        String id = mycomponent.value();
//        if (StringUtils.isEmpty(id)) {
//            id = MyStringUtils.toLowerCase4Index(id);
//        }
//        map.put(id, o);
//        map.put(clazz.getName(), o);
//
//        resolveAutowiredProperties(clazz, o);
    }

    private static void resolveAutowiredProperties(Class clazz, Object o) throws IllegalAccessException {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            Annotation[] annotations = field.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation instanceof MyAutowired) {
                    MyAutowired myAutowired = (MyAutowired) annotation;

                    String id = myAutowired.value();

                    Object dependObject = getMyBeanFromCache(field, id);
                    field.setAccessible(true);
                    field.set(o, dependObject);

                }

            }
        }
    }

    private static Object getMyBeanFromCache(Field field, String id) throws IllegalAccessException {
        Object dependObject = map.get(id);
        if (null == dependObject) {
            Collection<Object> list = map.values();
            Class fieldClazz = field.getType();
            for (Object o1 : list) {
                if (o1.getClass().isAssignableFrom(fieldClazz)) {
                    return o1;
                }
            }
            if (null == dependObject) {
                Collection<Class> annootationList = myAnnotationClazzMap.values();
                for (Class o1 : annootationList) {
                    if (fieldClazz == o1 || fieldClazz.isAssignableFrom(o1)) {
                        initializeClazz(o1);
                        return clazzmap.get(o1.getName());
                    }
                }
            }
        }
        return null;
    }

    private static String transFileToClassName(File file) {
        ClassLoader classLoader = BeanFactory.class.getClassLoader();
        String resources = classLoader.getResource("").getPath();
        String fileName = file.getName();
        String name = file.getPath();
        if(resources.charAt(0) != (name.charAt(0))){
            resources = resources.substring(1);
        }
        name = name.substring(resources.length());
        name = name.replace(File.separatorChar, '.');
        name = name.substring(0, name.lastIndexOf('.'));
        return name;


    }

    private static void getAllClassFile(File file) {
        if (file.isDirectory()) {
            for (File tmp : file.listFiles()) {
                getAllClassFile(tmp);
            }
        }
        if (file.isFile() && file.getName().endsWith(".class")) {
            String className = transFileToClassName(file);
            classPathClassNameLists.add(className);
        }
    }

    private static void resolveBeanXML() {
        // 任务一：读取解析xml，通过反射技术实例化对象并且存储待用（map集合）
        // 加载xml
        InputStream resourceAsStream = BeanFactory.class.getClassLoader().getResourceAsStream("beans.xml");
        // 解析xml
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();
            List<Element> beanList = rootElement.selectNodes("//bean");
            for (int i = 0; i < beanList.size(); i++) {
                Element element = beanList.get(i);
                // 处理每个bean元素，获取到该元素的id 和 class 属性
                String id = element.attributeValue("id");        // accountDao
                String clazz = element.attributeValue("class");  // com.lagou.edu.dao.impl.JdbcAccountDaoImpl
                // 通过反射技术实例化对象
                Class<?> aClass = Class.forName(clazz);
                Object o = aClass.newInstance();  // 实例化之后的对象

                // 存储到map中待用
                map.put(id, o);

            }

            // 实例化完成之后维护对象的依赖关系，检查哪些对象需要传值进入，根据它的配置，我们传入相应的值
            // 有property子元素的bean就有传值需求
            List<Element> propertyList = rootElement.selectNodes("//property");
            // 解析property，获取父元素
            for (int i = 0; i < propertyList.size(); i++) {
                Element element = propertyList.get(i);   //<property name="AccountDao" ref="accountDao"></property>
                String name = element.attributeValue("name");
                String ref = element.attributeValue("ref");

                // 找到当前需要被处理依赖关系的bean
                Element parent = element.getParent();

                // 调用父元素对象的反射功能
                String parentId = parent.attributeValue("id");
                Object parentObject = map.get(parentId);
                // 遍历父对象中的所有方法，找到"set" + name
                Method[] methods = parentObject.getClass().getMethods();
                for (int j = 0; j < methods.length; j++) {
                    Method method = methods[j];
                    if (method.getName().equalsIgnoreCase("set" + name)) {  // 该方法就是 setAccountDao(AccountDao accountDao)
                        method.invoke(parentObject, map.get(ref));
                    }
                }

                // 把处理之后的parentObject重新放到map中
                map.put(parentId, parentObject);

            }


        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

}
