package com.lagou.edu.utils;

import com.lagou.edu.annotation.MyTransactional;
import com.lagou.edu.service.impl.TransferServiceImpl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class AnnotationUtils {
    public static boolean isTransactionalAnnotationPresentOnMethod(Method method){
        Annotation[] methodAnnotations = method.getAnnotations();
        for (Annotation annotation : methodAnnotations) {
            if (annotation instanceof MyTransactional) {
                return true;
            }
        }
        return false;
    }

    public static boolean isTransactionalAnnotationPresentOnType(Class clazz){
        Annotation[] methodAnnotations = clazz.getAnnotations();
        for (Annotation annotation : methodAnnotations) {
            if (annotation instanceof MyTransactional) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Class clazz = TransferServiceImpl.class;
        System.out.println(isTransactionalAnnotationPresentOnType(clazz));
        for (Method method : clazz.getDeclaredMethods()){
            System.out.println(method.getName() + " : " + isTransactionalAnnotationPresentOnMethod(method));
        }
    }
}
