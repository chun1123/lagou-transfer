package com.lagou.edu.utils;

public class MyStringUtils {
    /**
     * 首字母大写
     *
     * @param string
     * @return
     */
    public static String toUpperCase4Index(String string) {
        char[] methodName = string.toCharArray();
        methodName[0] = toUpperCase(methodName[0]);
        return String.valueOf(methodName);
    }

    /**
     * 字符转成大写
     *
     * @param chars
     * @return
     */
    public static char toUpperCase(char chars) {
        if (97 <= chars && chars <= 122) {
            chars ^= 32;
        }
        return chars;
    }
    /**
     * 首字母大写
     *
     * @param string
     * @return
     */
    public static String toLowerCase4Index(String string) {
        char[] methodName = string.toCharArray();
        methodName[0] = toLowerCase(methodName[0]);
        return String.valueOf(methodName);
    }

    /**
     * 字符转成大写
     *
     * @param chars
     * @return
     */
    public static char toLowerCase(char chars) {
        if (65 <= chars && chars <= 90) {
            chars ^= 32;
        }
        return chars;
    }

    public static void main(String[] args) {
        String str1 = "a";
        System.out.println('A' - 0);
        System.out.println(toLowerCase('A'));//111 1111
    }
}
